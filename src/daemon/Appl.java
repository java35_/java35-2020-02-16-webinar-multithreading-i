package daemon;

public class Appl {

	public static void main(String[] args) {
		Thread t = new Thread(new DaemonThread());
		t.setDaemon(true);
		t.start();
	}
}
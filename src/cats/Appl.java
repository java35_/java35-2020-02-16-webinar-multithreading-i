package cats;

import java.time.Instant;

public class Appl {
	public static void main(String[] args) throws InterruptedException {
		Thread t1 = new Thread(new Cat(), "1");
		Thread t2 = new Thread(new Cat(), "2");
		Thread t3 = new Thread(new Cat(), "3");
		
		System.out.println(t1.getState());
		
		Instant start = Instant.now();
		t1.start();
		Thread.sleep(1000);
		t1.interrupt();
		System.out.println(t1.isInterrupted());
		System.out.println(t1.getState());
		t2.start();
		t3.start();
		t1.join();
		System.out.println("Main thread state: " + 
					Thread.currentThread().getState());
		System.out.println(t1.getState());
		t2.join();
		t3.join();
		Instant end = Instant.now();
		
		
		System.out.println("Main");
	}
}
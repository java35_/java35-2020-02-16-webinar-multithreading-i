package cockroach;

public class Race {

	public static void main(String[] args) {
		int size = 100000;
		
		Thread[] array = new Thread[size];
		
		for (int i = 0; i < size; i++) {
			array[i] = new Thread(new Cockroach(Thread.currentThread()), 
											"" + (i + 1));
			array[i].setDaemon(true);
		}
		
		for (Thread thread : array) {
			thread.start();
		}
		
//		for (Thread thread : array) {
//			if (!thread.isAlive())
//				return;
//		}
		try {
			for (Thread thread : array) {
				thread.join();
			}
		} catch (InterruptedException e) {
				e.printStackTrace();
		}
//		System.out.println(Cockroach.winner);
	}

}

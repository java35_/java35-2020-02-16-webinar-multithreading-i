package cockroach;

import java.time.Instant;
import java.util.Random;

public class Cockroach implements Runnable {
	static Random random = new Random();
	volatile static String winner = null;
	
	Instant finish;
	
	Thread mainThread;
	
	public Cockroach(Thread mainThread) {
		this.mainThread = mainThread;
	}
	
	@Override
	public void run() {
		// 2 3 4 5
		// 0 1 2 3
		for (int i = 0; i < 100; i++) {
			if (winner != null)
				mainThread.interrupt();
			int rand = random.nextInt(4) + 2;
			try {
				Thread.sleep(rand); // 2 - 5
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (winner == null) {
			winner = Thread.currentThread().getName();
			System.out.println(winner);
			mainThread.interrupt();
		}
//		finish = Instant.now();
	}

}
